+++
date = "2019-01-22T05:00:00+00:00"
excludeFromTopNav = false
showDate = false
title = "Projects"

+++
## Kronos

### The Discord moderation bot with persistence

Kronos is a reboot of the OtmasBot of days gone by. The original OtmasBot was a poorly coded mess, but (for whatever reason) it worked, so I'm trying to rewrite it with good practices and standards.