---
title: "HakkardMUSH"
date: 2018-10-25T13:49:39-04:00
draft: false

categories: ["announcement"]
tags: []
author: "DevOsmium"
---
Hakkard is something that I've been working on for a while. It started life as a Dungeons and Dragons campaign setting, which can still be found [here](https://trello.com/b/jNEzwwAq). But then, I thought to myself, it's not as much fun if the true Hakkard experience, which can only be received by the creator, is only available to a select few people who know me in real life. Why don't I make a *playable* version of Hakkard online, where anyone can come and enjoy it the way I do? That's why I'm here today, to announce the **extreme** early alpha of Hakkard!

## But what's an MU*?
In short, an MU* is a text-based, online RPG that formed the foundations of modern MMORPGs.

The long form of it is found on [Wikipedia](https://en.wikipedia.org/wiki/MUD)

## How do I connect?
It's very simple! Go to our subdomain [http://hakkard.devosmium.xyz](http://hakkard.devosmium.xyz) and click the big "Play Online" button at the top! All the information is right there ready for you.
